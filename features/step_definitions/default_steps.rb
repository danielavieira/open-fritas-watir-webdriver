# encoding: utf-8

Dado(/^que eu esteja no site do Abril ID$/) do
  @browser.goto "http://id.abril.com.br/"
end

Dado(/^eu preencho o campo usuário com o valor "(.*?)"$/) do |login|
  @browser.text_field(:id, "login").set login
end

Dado(/^eu preencho o campo senha com o valor "(.*?)"$/) do |senha|
    @browser.text_field(:id, "senha").set senha
end

Dado(/^eu clico no botao Entrar$/) do
    @browser.input(:id, "loginEnviar").click
end

Entao(/^eu devo ver o nome completo do usuário "(.*?)"$/) do |nome_completo|
	sleep 4
  fail "Nome completo nao encontrado." unless @browser.div(:class, "userdata-box-realname").text == nome_completo
end