require 'watir-webdriver'

Before do


tokens = { 'browserstack' => 'https://TestesdoServico:KXhJa4coi54TAsus1msb@hub.browserstack.com/wd/hub',
			'testingbot' => 'http://6056c7d6acea1c7de329d78b6c14c75f:f68f7ec142306aaf9fa58ddbdf4409e4@hub.testingbot.com:80/wd/hub',
			'saucelabs' => 'http://karolineleite:b6ae7dd9-472b-40a3-86ff-f6e4e6b7ee80@ondemand.saucelabs.com:80/wd/hub'}

browser = {
	'firefox' => 
		Selenium::WebDriver::Remote::Capabilities.firefox,
	'internet_explorer' =>
		Selenium::WebDriver::Remote::Capabilities.internet_explorer,
	'chrome' =>
		Selenium::WebDriver::Remote::Capabilities.chrome,
	'safari' =>
		Selenium::WebDriver::Remote::Capabilities.safari,
	'iphone' =>
		Selenium::WebDriver::Remote::Capabilities.iphone
}

caps = browser[BROWSER] #Selenium::WebDriver::Remote::Capabilities.internet_explorer
caps.version = BROWSER_VERSION
caps.platform = SO.to_sym

#caps[:name] = "Testing Selenium 2 with Ruby on BrowserStack"

  @browser = Watir::Browser.new(
  	:remote,
    :url => tokens[HUB],
    :desired_capabilities => caps)
end

After do
  @browser.quit
end


